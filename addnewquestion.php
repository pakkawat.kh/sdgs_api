<?php
require_once('connection.php');
$_POST = json_decode(file_get_contents("php://input"),true);
$id = $_POST['id'];
$moduleid = $_POST['moduleid'];
$question = $_POST['question'];
$choice1 = $_POST['choice1'];
$choice2 = $_POST['choice2'];
$choice3 = $_POST['choice3'];
$choice4 = $_POST['choice4'];
$correctanswer = $_POST['correctanswer'];

$db->insert("question",[
"moduleid"=>$moduleid,
"question"=>$question,
"choice1"=>$choice1,
"choice2"=>$choice2,
"choice3"=>$choice3,
"choice4"=>$choice4,
"correctanswer"=>$correctanswer
]);
//update total in question table
$resultCount = $db->count("question",[
    "moduleid"=>$moduleid
]);
$db->update("module",[
    "total"=>$resultCount
],[
    "id"=>$moduleid
]);
echo "finish";
?>